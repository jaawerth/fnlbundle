(local (fmt searchpath) (values string.format package.searchpath))
(var *ns-module-root* nil)
(λ *ns-module-root! [?val]
  "Sets or gets the name of the root module name; used for namespacing
require-import. Can only be set once, before `require-include` is ever used."
  (if (not ?val) *ns-module-root*
      (do (assert (not *ns-module-root*)
                  "module-root-name! can only be called once")
        (set *ns-module-root* (.. :# ?val))
        ?val)))

(fn resolve [mod-name]
  (or (searchpath mod-name package.path) (searchpath mod-name fennel.path)))

(λ prefix [name]
  (if (not *ns-module-root*) name
      (string.format "fnl-module#%s#%x.%s" *ns-module-root*
                     (math.random (^ 2 32)) name)))

(local preloads {})
(λ require-include [module-name ?fallback]
  "Attempt to inline module code at compile time and patch package.preload 
before requiring. Will fallback to plain require if ?fallback == true."
  (assert (= :string (type module-name)) "`modulename` must be a string")
  (let [alias (prefix module-name)
        mod-path (doto (resolve module-name))
        compile (if (-?> mod-path (: :sub -4) (= :.fnl))
                    fennel.compileString #$)
        module-txt (when (not (. preloads module-name))
                         (-?> mod-path io.open (: :read "*a") compile))
        includer   `(tset package.preload ,alias (loadstring ,module-txt))
        out `(let [require# require] (require# ,alias))]
    (assert (or module-txt (. preloads module-name) ?fallback)
            (fmt "module '%s' could not be inlined and ?fallback not set" module-name))
    (when module-txt (tset preloads module-name alias)
                     (table.insert out 3 includer))
    out))

(fn *patch-require! []
  "monkeypatch require so it attempts to bundle instead. Only invoke this
in a config file loaded with -l when targeting an entrypoint for bundling."
  (var root-scope _SCOPE)
  (while root-scope.parent (set root-scope root-scope.parent))
  (set root-scope.specials.require _SCOPE.specials.require-include))

{: require-include : *ns-module-root! : *patch-require!}
