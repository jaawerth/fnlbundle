(local foo [:FOO 1])
(local baz (require :example.baz))
(local bar (require :example.bar))

(print "foo requires bar, and bar requires and inlines baz.")
(each [i [k entry] (ipairs [[:foo foo] [:bar bar] [:baz baz]])]
  (print (.. k ":  " (table.concat entry " - "))))
