# fnlbundle - an experimental fennel bundler

## macros
### `require-include`
Sometimes you want only a single file to include in projects. The `require-include` macro (name
may change!) works like Fennel's `include` macro, only when  a given module is included for the first
it sets it on `package.preload`, then uses Lua's normal `require` function. This way, all subsequent requires
can simply grab the code from the package cache normally.

### `*patch-require!`

This macro monkey-patches `<root-scope>.specials.require` with `require-include`, allowing you
to attempt bundling an entire project from an entrypoint.

**Note:** this will only work on Fennel code
for the time being. Similarly shadowing `require` in Lua would require the use of a Lua parser.

## Example

These files are all in the [example](example/) directory.

```clojure
; -- config (example/config.fnl)
; This should be included first, either with `fennel -l path/to/config.fnl`
; (`-l` is available starting in Fennel 0.3.1), or it can be done inline or
; required from the entrypoint you target.
(require-macros :require-include)
(*patch-require!)
; Now, all `require calls will try to inline and patch `package.preload`.

; -- foo.fnl
(local foo [:FOO 1])
(local baz (require :example.baz))
(local bar (require :example.bar))

(print "foo requires bar, and bar requires and inlines baz.")
(each [i [k entry] (ipairs [[:foo foo] [:bar bar] [:baz baz]])]
  (print (.. k ":  " (table.concat entry " - "))))

; -- bar.fnl
(local bar [:BAR 2])
(each [_ v (ipairs (require :example.baz))]
  (table.insert bar v))
bar

; -- baz.fnl
[:BAZ 3]
```

Now, in Fennel **0.3.1** or newer, run: 
```bash
fennel -l example/config.fnl --compile example/foo.fnl
```

### Output
```lua
local foo = {"FOO", 1}
local baz = baz
do
  local require_23_0_ = require
  package.preload["example.baz"] = loadstring("return {\"BAZ\", 3}")
  baz = require_23_0_("example.baz")
end
local bar = bar
do
  local require_23_0_ = require
  package.preload["example.bar"] = loadstring("local bar = {\"BAR\", 2}\nlocal function _0_(...)\n  local require_23_0_ = require\n  return require_23_0_(\"example.baz\")\nend\nfor _, v in ipairs(_0_(...)) do\n  table.insert(bar, v)\nend\nreturn bar")
  bar = require_23_0_("example.bar")
end
print("foo requires bar, and bar requires and inlines baz.")
for i, _0_0 in ipairs({{"foo", foo}, {"bar", bar}, {"baz", baz}}) do
  local _1_ = _0_0
  local k = _1_[1]
  local entry = _1_[2]
  print((k .. ":  " .. table.concat(entry, " - ")))
end
return nil
```
